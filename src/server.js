const{ request } = require('express');
const express = require('express');
const converter = require('./converter');
const app = express();
const port = 3000;

app.get('/', (req, res) => res.send("Welcome!"));

// RGB to Hex endpoint
app.get('/rgb-to-hex', (req,res) => {
    const red = parseInt(req.query.red, 10);
    const green = parseInt(req.query.green, 10);
    const blue = parseInt(req.query.blue);
    const hex = converter.rgbToHex(red, green, blue);
    res.send(hex);
}); 

app.get('/hexToRgb', (req,res) => {
    var hex = req.query.hex;
    var rgb = converter.hexToRgb(hex);
    res.send(JSON.stringify(rgb));
}); 

console.log("NODE_ENV: " + process.env.NODE_ENV);
if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(port, () => console.log("Server listening"));
}

